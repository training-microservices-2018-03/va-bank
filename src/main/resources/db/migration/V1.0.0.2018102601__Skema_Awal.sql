create table virtual_account (
  id varchar(36),
  nomor_virtual_account varchar (50) not null,
  nomor_tagihan varchar (255) not null,
  nama varchar (255) not null,
  nilai decimal (19,2) not null,
  primary key (id)
);

create table payment (
  id varchar(36),
  id_virtual_account varchar(36) not null,
  nilai decimal (19,2) not null,
  waktu_transaksi timestamp not null,
  referensi varchar (255) not null,
  primary key (id),
  foreign key (id_virtual_account) references virtual_account (id)
);