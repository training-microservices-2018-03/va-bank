package com.artivisi.training.microservice201803.vabank.dto;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class Pay {
    private String rekening;
    private BigDecimal nilai;
}
