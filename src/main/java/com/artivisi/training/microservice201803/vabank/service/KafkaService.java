package com.artivisi.training.microservice201803.vabank.service;

import com.artivisi.training.microservice201803.vabank.dao.PaymentDao;
import com.artivisi.training.microservice201803.vabank.dao.VirtualAccountDao;
import com.artivisi.training.microservice201803.vabank.dto.Pay;
import com.artivisi.training.microservice201803.vabank.dto.VaPayment;
import com.artivisi.training.microservice201803.vabank.dto.VaRequest;
import com.artivisi.training.microservice201803.vabank.dto.VaResponse;
import com.artivisi.training.microservice201803.vabank.entity.Payment;
import com.artivisi.training.microservice201803.vabank.entity.VirtualAccount;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.UUID;

@Service
@Transactional
public class KafkaService {

    private static final Logger LOGGER = LoggerFactory.getLogger(KafkaService.class);

    @Value("${va.bank.id}")
    private String idBank;
    @Value("${va.bank.kode}")
    private String kodeBank;
    @Value("${kafka.topic.va-response}")
    private String topicVaResponse;
    @Value("${kafka.topic.va-payment}")
    private String topicVaPayment;

    @Autowired
    private KafkaTemplate<String, String> kafkaTemplate;
    @Autowired
    private ObjectMapper objectMapper;
    @Autowired
    private VirtualAccountDao virtualAccountDao;
    @Autowired
    private PaymentDao paymentDao;

    @KafkaListener(topics = "${kafka.topic.va-request}")
    public void handleVaRequest(String message) {
        LOGGER.debug("Terima message [{}] dari topic va-request", message);

        try {
            VaRequest vaRequest = objectMapper.readValue(message, VaRequest.class);

            VirtualAccount va = VirtualAccount.builder()
                    .nomorTagihan(vaRequest.getNomorTagihan())
                    .nama(vaRequest.getNama())
                    .nilai(vaRequest.getNilai())
                    .nomorVirtualAccount(kodeBank + vaRequest.getNomorTagihan())
                    .build();

            virtualAccountDao.save(va);

            // kirim notifikasi sukses
            VaResponse vaResponse = VaResponse.builder()
                    .bank(idBank)
                    .nomorTagihan(vaRequest.getNomorTagihan())
                    .nomorVirtualAccount(va.getNomorVirtualAccount())
                    .build();

            String response = objectMapper.writeValueAsString(vaResponse);
            LOGGER.debug("Kirim response [{}] ke topic va-response", response);
            kafkaTemplate.send(topicVaResponse, response);
        } catch (IOException e) {
            LOGGER.error(e.getMessage(), e);
        }

    }

    public void process(Pay pay) {
        virtualAccountDao.findByNomorVirtualAccount(pay.getRekening())
                .ifPresent(va -> {
                    Payment payment = new Payment();
                    payment.setVirtualAccount(va);
                    payment.setNilai(pay.getNilai());
                    payment.setReferensi(UUID.randomUUID().toString());
                    payment.setWaktuTransaksi(LocalDateTime.now());

                    paymentDao.save(payment);
                    kirimNotifikasiPembayaran(payment);
                });
    }

    private void kirimNotifikasiPembayaran(Payment payment) {
        try {
            VaPayment vaPayment = VaPayment.builder()
                    .bank(idBank)
                    .nilai(payment.getNilai())
                    .nomorVirtualAccount(payment.getVirtualAccount().getNomorVirtualAccount())
                    .referensi(payment.getReferensi())
                    .waktuTransaksi(payment.getWaktuTransaksi())
                    .build();

            String json = objectMapper.writeValueAsString(vaPayment);

            LOGGER.debug("Kirim notifikasi payment [{}] ke topic va-payment", json);
            kafkaTemplate.send(topicVaPayment, json);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

    }
}
