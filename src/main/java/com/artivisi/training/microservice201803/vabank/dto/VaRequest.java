package com.artivisi.training.microservice201803.vabank.dto;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class VaRequest {
    private String nomorTagihan;
    private String nama;
    private BigDecimal nilai;
}