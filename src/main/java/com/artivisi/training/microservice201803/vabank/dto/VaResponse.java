package com.artivisi.training.microservice201803.vabank.dto;

import lombok.Builder;
import lombok.Data;

@Data @Builder
public class VaResponse {
    private String nomorTagihan;
    private String bank;
    private String nomorVirtualAccount;
}