package com.artivisi.training.microservice201803.vabank.dao;

import com.artivisi.training.microservice201803.vabank.entity.VirtualAccount;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface VirtualAccountDao extends CrudRepository<VirtualAccount, String> {
    Optional<VirtualAccount> findByNomorVirtualAccount(String rekening);
}
