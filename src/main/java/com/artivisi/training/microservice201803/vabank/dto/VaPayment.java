package com.artivisi.training.microservice201803.vabank.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Builder;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data @Builder
public class VaPayment {
    private String bank;
    private String nomorVirtualAccount;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime waktuTransaksi;
    private String referensi;
    private BigDecimal nilai;
}