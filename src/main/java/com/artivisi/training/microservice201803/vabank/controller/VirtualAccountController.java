package com.artivisi.training.microservice201803.vabank.controller;

import com.artivisi.training.microservice201803.vabank.dao.VirtualAccountDao;
import com.artivisi.training.microservice201803.vabank.entity.VirtualAccount;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/va")
public class VirtualAccountController {

    @Autowired private VirtualAccountDao virtualAccountDao;

    @GetMapping("/")
    public Iterable<VirtualAccount> findAll(){
        return virtualAccountDao.findAll();
    }
}
