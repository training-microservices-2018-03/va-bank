package com.artivisi.training.microservice201803.vabank.dao;

import com.artivisi.training.microservice201803.vabank.entity.Payment;
import org.springframework.data.repository.CrudRepository;

public interface PaymentDao extends CrudRepository<Payment, String> {
}
