package com.artivisi.training.microservice201803.vabank;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VaBankApplication {

	public static void main(String[] args) {
		SpringApplication.run(VaBankApplication.class, args);
	}
}
