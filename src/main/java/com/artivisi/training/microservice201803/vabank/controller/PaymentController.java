package com.artivisi.training.microservice201803.vabank.controller;

import com.artivisi.training.microservice201803.vabank.dao.PaymentDao;
import com.artivisi.training.microservice201803.vabank.dto.Pay;
import com.artivisi.training.microservice201803.vabank.entity.Payment;
import com.artivisi.training.microservice201803.vabank.service.KafkaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController @RequestMapping("/payment")
public class PaymentController {

    @Autowired private PaymentDao paymentDao;
    @Autowired private KafkaService kafkaService;

    @PostMapping("/")
    @ResponseStatus(HttpStatus.CREATED)
    public void pay(@RequestBody @Valid Pay pay) {
        kafkaService.process(pay);
    }

    @GetMapping("/")
    public Iterable<Payment> get(){
        return paymentDao.findAll();
    }
}
